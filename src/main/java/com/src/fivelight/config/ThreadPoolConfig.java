package com.src.fivelight.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 线程池配置
 */
@Component
public class ThreadPoolConfig {
    /**
     * 设置核心线程数
     */
    @Value("${thread-pool-config.core-pool-size}")
    private int corePoolSize = 5;
    /**
     * 设置最大线程数
     */
    @Value("${thread-pool-config.max-pool-size}")
    private int maxPoolSize = 10;
    /**
     * 设置队列容量
     */
    @Value("${thread-pool-config.queue-capacity}")
    private int queueCapacity = 20;
    /**
     * 设置线程活跃时间（秒）
     */
    @Value("${thread-pool-config.keep-alive-seconds}")
    private int keepAliveSeconds = 60;
    /**
     * 设置默认线程名称
     */
    @Value("${thread-pool-config.thread-name-prefix}")
    private String threadNamePrefix = "back-";

    public int getCorePoolSize() {
        return corePoolSize;
    }

    public void setCorePoolSize(int corePoolSize) {
        this.corePoolSize = corePoolSize;
    }

    public int getMaxPoolSize() {
        return maxPoolSize;
    }

    public void setMaxPoolSize(int maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    public int getQueueCapacity() {
        return queueCapacity;
    }

    public void setQueueCapacity(int queueCapacity) {
        this.queueCapacity = queueCapacity;
    }

    public int getKeepAliveSeconds() {
        return keepAliveSeconds;
    }

    public void setKeepAliveSeconds(int keepAliveSeconds) {
        this.keepAliveSeconds = keepAliveSeconds;
    }

    public String getThreadNamePrefix() {
        return threadNamePrefix;
    }

    public void setThreadNamePrefix(String threadNamePrefix) {
        this.threadNamePrefix = threadNamePrefix;
    }
}
