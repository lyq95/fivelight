package com.src.fivelight.bean;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.gr.zyg.core.account.GsAccountToken;
import com.gr.zyg.core.jwt.JWTHelper;
import com.gr.zyg.waregouseapi.UploadFile;
import com.gr.zyg.waregouseapi.config.WarehouseConfig;
import com.gr.zyg.waregouseapi.realization.DefUploadFile;
import com.src.fivelight.config.ThreadPoolConfig;
import com.src.fivelight.useraccount.dao.UserDao;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 这里是所有的Bean集合
 */
@Configuration
public class Beans {
    /**
     * json 解析配置
     *
     * @return 解析对象至Json
     */
    @Bean
    public HttpMessageConverters fastjsonHttpMessageConverter() {
        // 定义一个转换消息的对象
        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();

        // 添加fastjson的配置信息 比如 ：是否要格式化返回的json数据
        FastJsonConfig fastJsonConfig = new FastJsonConfig();

        fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat);

        // 时间
        fastJsonConfig.setDateFormat("yyyy-MM-dd HH:mm:ss");

        // UTF-8编码
        fastJsonConfig.setCharset(StandardCharsets.UTF_8);

        // 在转换器中添加配置信息
        fastConverter.setFastJsonConfig(fastJsonConfig);

        HttpMessageConverter<?> converter;
        converter = fastConverter;

        return new HttpMessageConverters(converter);
    }

    /**
     * 设置springBoot上传的文件
     *
     * @return Apache Upload 组件
     */
    @Bean
    public CommonsMultipartResolver commonsMultipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setDefaultEncoding("UTF-8");
        return multipartResolver;
    }

    /**
     * 回调线程池
     *
     * @param threadPoolConfig 线程池配置
     * @return
     */
    @Bean
    public Executor taskExecutor(ThreadPoolConfig threadPoolConfig) {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        // 设置核心线程数
        executor.setCorePoolSize(threadPoolConfig.getCorePoolSize());
        // 设置最大线程数
        executor.setMaxPoolSize(threadPoolConfig.getMaxPoolSize());
        // 设置队列容量
        executor.setQueueCapacity(threadPoolConfig.getQueueCapacity());
        // 设置线程活跃时间（秒）
        executor.setKeepAliveSeconds(threadPoolConfig.getKeepAliveSeconds());
        // 设置默认线程名称
        executor.setThreadNamePrefix(threadPoolConfig.getThreadNamePrefix() + "-");
        // 设置拒绝策略
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        // 等待所有任务结束后再关闭线程池
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.initialize();
        return executor;
    }

    /**
     * JWT初始化。
     * userToken。
     *
     * @return Token管理
     */
    @Bean
    public GsAccountToken doorGsAccountToken() {
        String secret = "BE121D15E63A3C4409FC7E6D139A5EBE";// 秘钥
        long expiresSecond = 604800000;// 存在时间毫秒
        String iss = "Server";// jwt签发者
        String sub = "User";// jwt所面向的用户
        String aud = "User";// 接收jwt的一方
        JWTHelper jwtHelper = new JWTHelper(secret, expiresSecond, iss, sub, aud);
        return new GsAccountToken(jwtHelper);
    }
}
