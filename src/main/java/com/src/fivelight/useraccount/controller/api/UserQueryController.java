package com.src.fivelight.useraccount.controller.api;

import com.gr.zyg.core.exception.Result;
import com.gr.zyg.core.exception.define.DefineException;
import com.src.fivelight.useraccount.server.UserBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 文件说明: 用户操作（只获取数据）
 * 版本号: v1.0.1
 * 编制者: lyq
 * 编制时间: 2020-02-09 12:43
 */
@RestController
@RequestMapping("/api/useraccount/query/")
public class UserQueryController {
    @Autowired
    private UserBiz userBiz;

    /**
     * 分页获取用户信息
     *
     * @param pageNo    当前页数
     * @param rowsCount 每页条数
     * @param findKey   查找关键字
     * @return 返回信息
     * @throws DefineException
     */
    @RequestMapping("getUserList")
    public Result getUserList(String pageNo, String rowsCount, String findKey) throws DefineException {
        return new Result(userBiz.getUserList(pageNo, rowsCount, findKey));
    }
}
