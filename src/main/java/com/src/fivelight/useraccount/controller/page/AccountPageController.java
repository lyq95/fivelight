package com.src.fivelight.useraccount.controller.page;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 文件说明: 用户操作（只跳转页面）
 * 版本号: v1.0.1
 * 编制者: lyq
 * 编制时间: 2020-02-09 12:43
 */
@Controller
@RequestMapping("/page/useraccount/")
public class AccountPageController {

    /**
     * 用户登录页面
     *
     * @return
     */
    @RequestMapping("login")
    public String login() {
        return "login";
    }
}
