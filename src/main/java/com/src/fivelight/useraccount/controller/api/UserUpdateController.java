package com.src.fivelight.useraccount.controller.api;

import com.gr.zyg.core.exception.AllException;
import com.gr.zyg.core.exception.Result;
import com.gr.zyg.core.exception.define.DefineException;
import com.src.fivelight.useraccount.parameter.PUser;
import com.src.fivelight.useraccount.server.UserBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * 文件说明: 用户操作（只变更数据）
 * 版本号: v1.0.1
 * 编制者: lyq
 * 编制时间: 2020-02-09 12:43
 */
@RestController
@RequestMapping("/api/account/update/")
public class UserUpdateController {
    @Autowired
    private UserBiz userBiz;

    /**
     * 用户登录
     *
     * @param session 会话
     * @param pUser   用户实体类
     * @return 返回信息
     * @throws DefineException
     */
    @RequestMapping("login")
    public Result login(HttpSession session, PUser pUser) throws DefineException {
        return new Result(AllException.SUCCESS, userBiz.login(session, pUser));
    }
}
