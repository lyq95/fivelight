package com.src.fivelight.useraccount.entity;

/**
 * 用于存储常量
 *
 * @author : lyq
 * 编制时间: 2020-02-09 12:43
 */
public class Const {
    public static final String USER = "user";
    public static final String USER_TOKEN = "userToken";
}
