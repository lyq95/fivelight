package com.src.fivelight.useraccount.dao;

import com.gr.zyg.core.date.Paging;
import com.src.fivelight.useraccount.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * 文件说明: 用户数据层
 * 版本号: v1.0.1
 * 编制者: lyq
 * 编制时间: 2020-02-09 12:43
 */
@Repository
public interface UserDao {
    /**
     * 插入用户
     *
     * @param user 用户信息
     * @return 返回信息
     */
    int insertUser(User user);

    /**
     * 用户管理列表
     *
     * @param paging 分页数据
     * @return 返回信息
     */
    List<User> getUserList(Paging paging);

    /**
     * 获取用户管理列表记录数
     *
     * @param paging 分页数据
     * @return 返回信息
     */
    int getUserListCount(Paging paging);

    /**
     * 通过open获取用户信息
     *
     * @param openId 用户唯一标识符
     * @return 返回信息
     */
    User getUserByOpenId(String openId);

    /**
     * 更新用户登录时间
     *
     * @param user 用户集合
     */
    void updateLastLoginDate(User user);
}
