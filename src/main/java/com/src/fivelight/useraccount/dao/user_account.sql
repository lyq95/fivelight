/*
Navicat MySQL Data Transfer

Source Server         : grshare12
Source Server Version : 50712
Source Host           : localhost:3306
Source Database       : fiveglight

Target Server Type    : MYSQL
Target Server Version : 50712
File Encoding         : 65001

Date: 2020-02-09 13:07:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user_account
-- ----------------------------
DROP TABLE IF EXISTS `user_account`;
CREATE TABLE `user_account` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户编号',
  `open_id` varchar(64) NOT NULL COMMENT '微信用户的唯一标识',
  `nick_name` varchar(25) NOT NULL COMMENT '用户昵称',
  `sex` int(11) NOT NULL COMMENT '用户的性别，值为1时是男性，值为2时是女性，值为0时是未知',
  `province` varchar(15) NOT NULL COMMENT '所在省份',
  `city` varchar(15) NOT NULL COMMENT '所在城市',
  `country` varchar(15) NOT NULL COMMENT '所在城市',
  `headimgurl` varchar(300) NOT NULL COMMENT '微信头像',
  `dream_value` int(11) NOT NULL DEFAULT '0' COMMENT '梦享值',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of user_account
-- ----------------------------
