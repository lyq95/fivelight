package com.src.fivelight.useraccount.parameter;

import com.gr.zyg.core.exception.ExceptionSuperManager;
import com.gr.zyg.core.exception.define.ParameterException;

/**
 * 文件说明: 收到的用户数据
 * 版本号: v1.0.1
 * 编制者: lyq
 * 编制时间: 2020-02-09 12:43
 */
public class PUser {
    /**
     * 微信用户的唯一标识
     */
    private String openId;
    /**
     * 用户昵称
     */
    private String nickName;
    /**
     * 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
     */
    private Integer sex;
    /**
     * 所在城市
     */
    private String country;
    /**
     * 所在省份
     */
    private String province;
    /**
     * 所在城市
     */
    private String city;
    /**
     * 微信头像
     */
    private String headimgurl;

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) throws ParameterException {
        ExceptionSuperManager.checkNoEmptyParmLenght(openId, 64, "openid不合法");
        this.openId = openId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) throws ParameterException {
        ExceptionSuperManager.checkNoEmptyParmLenght(nickName, 25, "昵称不合法");
        this.nickName = nickName;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) throws ParameterException {
        ExceptionSuperManager.checkNoEmptyParmLenght(country, 15, "区域不合法");
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) throws ParameterException {
        ExceptionSuperManager.checkNoEmptyParmLenght(province, 15, "省份不合法");
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) throws ParameterException {
        ExceptionSuperManager.checkNoEmptyParmLenght(city, 15, "城市不合法");
        this.city = city;
    }

    public String getHeadimgurl() {
        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl;
    }
}
