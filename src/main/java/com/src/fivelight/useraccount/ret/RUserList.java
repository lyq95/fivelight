package com.src.fivelight.useraccount.ret;


import com.gr.zyg.core.date.PagingInfo;

import java.util.List;

/**
 * 文件说明: 分页返回的数据
 * 版本号: v1.0.1
 * 编制者: lyq
 * 编制时间: 2020-02-09 12:43
 */
public class RUserList extends PagingInfo {

    private List<RUser> users;


    public List<RUser> getUsers() {
        return users;
    }

    public void setUsers(List<RUser> users) {
        this.users = users;
    }

}
