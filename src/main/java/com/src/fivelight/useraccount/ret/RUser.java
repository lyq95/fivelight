package com.src.fivelight.useraccount.ret;

import com.src.fivelight.useraccount.entity.User;

import java.util.Date;

/**
 * 文件说明:   用户信息
 * 版本号:     v1.0.0
 * 包名称:     com.src.fivelight.useraccount.ret
 * 项目名称:   filelight
 * 编制者      lyq
 * 编制时间: 2020-02-09 12:43
 */
public class RUser {
    /**
     * 用户id
     */
    private Integer userId;
    /**
     * 微信用户的唯一标识
     */
    private String openId;
    /**
     * 用户昵称
     */
    private String nickName;
    /**
     * 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
     */
    private Integer sex;
    /**
     * 所在城市
     */
    private String country;
    /**
     * 所在省份
     */
    private String province;
    /**
     * 所在城市
     */
    private String city;
    /**
     * 微信头像
     */
    private String headimgurl;
    /**
     * 梦享值
     */
    private Integer dreamValue;
    /**
     * 创建时间
     */
    private Date createDate;
    /**
     * 更新时间
     */
    private Date updateDate;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getHeadimgurl() {
        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl;
    }

    public Integer getDreamValue() {
        return dreamValue;
    }

    public void setDreamValue(Integer dreamValue) {
        this.dreamValue = dreamValue;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public void toRUser(User user) {
        this.setUserId(user.getUserId());
        this.setOpenId(user.getOpenId());
        this.setNickName(user.getNickName());
        this.setSex(user.getSex());
        this.setCountry(user.getCountry());
        this.setProvince(user.getProvince());
        this.setCity(user.getCity());
        this.setHeadimgurl(user.getHeadimgurl());
        this.setDreamValue(user.getDreamValue());
        this.setCreateDate(user.getCreateDate());
        this.setUpdateDate(user.getUpdateDate());
    }
}
