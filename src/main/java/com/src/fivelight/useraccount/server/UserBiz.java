package com.src.fivelight.useraccount.server;

import com.gr.zyg.core.exception.define.DefineException;
import com.gr.zyg.core.exception.define.ParameterException;
import com.src.fivelight.useraccount.entity.User;
import com.src.fivelight.useraccount.parameter.PUser;
import com.src.fivelight.useraccount.ret.RUserList;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * 文件说明: 用户业务层接口
 * 版本号: v1.0.1
 * 编制者: lyq
 * 编制时间: 2020-02-09 12:43
 */
public interface UserBiz {

    /**
     * 用户登录
     *
     * @param session 会话
     * @param pUser   用户实体类
     * @return 返回信息
     * @throws DefineException
     */
    Map<String, Object> login(HttpSession session, PUser pUser) throws DefineException;

    /**
     * 分页获取集合
     *
     * @param pageNo    当前页数
     * @param rowsCount 每页条数
     * @param findKey   查找关键字
     * @return 返回信息
     */
    RUserList getUserList(String pageNo, String rowsCount, String findKey) throws DefineException;

    /**
     * 登陆时间
     *
     * @param user 用户集合
     */
    void updateLastLoginDate(User user);

    /**
     * 录入用户
     *
     * @param user 用户实体类
     */
    void inserUser(User user);
}
