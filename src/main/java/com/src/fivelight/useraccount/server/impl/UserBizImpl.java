package com.src.fivelight.useraccount.server.impl;

import com.gr.zyg.core.account.GsAccountToken;
import com.gr.zyg.core.account.UserToken;
import com.gr.zyg.core.date.Paging;
import com.gr.zyg.core.exception.ExceptionSuperManager;
import com.gr.zyg.core.exception.define.DefineException;
import com.gr.zyg.core.verification.ValidatorUtils;
import com.src.fivelight.useraccount.dao.UserDao;
import com.src.fivelight.useraccount.entity.Const;
import com.src.fivelight.useraccount.entity.User;
import com.src.fivelight.useraccount.parameter.PUser;
import com.src.fivelight.useraccount.ret.RUser;
import com.src.fivelight.useraccount.ret.RUserList;
import com.src.fivelight.useraccount.server.UserBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * 文件说明: 用户业务层
 * 版本号: v1.0.1
 * 编制者: lyq
 * 编制时间: 2020-02-09 12:43
 */
@Service
public class UserBizImpl implements UserBiz {
    @Autowired
    private UserDao userDao;
    @Autowired
    private GsAccountToken gsAccountToken;

    /**
     * 登录
     *
     * @param session 会话
     * @param pUser   用户实体类
     * @return 返回信息
     * @throws DefineException
     */
    @Override
    public Map<String, Object> login(HttpSession session, PUser pUser) throws DefineException {
        User user = userDao.getUserByOpenId(pUser.getOpenId());
        if (user == null) {
            user.setOpenId(pUser.getOpenId());
            user.setNickName(pUser.getNickName());
            user.setSex(pUser.getSex());
            user.setProvince(pUser.getProvince());
            user.setCity(pUser.getCity());
            user.setCountry(pUser.getCountry());
            user.setHeadimgurl(pUser.getHeadimgurl());
            user.setDreamValue(0);
            this.inserUser(user);
        } else {
            this.updateLastLoginDate(user);
        }
        // 生成Token
        UserToken userToken = gsAccountToken.issuedToken(user.getUserId());
        RUser rUser = new RUser();
        rUser.toRUser(user);
        Map<String, Object> map = new HashMap<>();
        map.put(Const.USER, rUser);
        map.put(Const.USER_TOKEN, userToken.getUserToken());
        session.setAttribute(Const.USER, user);
        session.setAttribute(Const.USER_TOKEN, userToken.getUserToken());
        return map;
    }

    /**
     * 更新登陆时间
     */
    @Override
    public void updateLastLoginDate(User user) {
        userDao.updateLastLoginDate(user);
    }

    /**
     * 录入用户
     *
     * @param user 用户实体类
     */
    @Override
    public void inserUser(User user) {
        userDao.insertUser(user);
    }

    /**
     * 分页获取集合
     *
     * @param pageNo    当前页数
     * @param rowsCount 每页条数
     * @param findKey   查找关键字
     * @return 返回信息
     */
    @Override
    public RUserList getUserList(String pageNo, String rowsCount, String findKey) throws DefineException {
        Paging paging = new Paging(ExceptionSuperManager.checkNotEmptyAbove0Int(pageNo, "pageNO不合法"),
                ExceptionSuperManager.checkNotEmptyAbove0Int(rowsCount, "rowsCount不合法"));
        if (!ValidatorUtils.isEmpty(findKey)) {
            paging.getData().put("findKey", "%" + findKey + "%");
        }
        paging.setSum(userDao.getUserListCount(paging));
        List<User> users = userDao.getUserList(paging);
        List<RUser> rUsers = new ArrayList<>();
        RUser rUser = null;
        for (User user : users) {
            rUser = new RUser();
            rUser.toRUser(user);
            rUsers.add(rUser);
        }
        RUserList rUserManagement = new RUserList();
        rUserManagement.setUsers(rUsers);
        rUserManagement.setData(paging.toPagingInfo());
        return rUserManagement;
    }
}
