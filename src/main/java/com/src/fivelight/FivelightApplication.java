package com.src.fivelight;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.MultipartAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@SpringBootApplication(exclude = {MultipartAutoConfiguration.class})
@EnableAsync
@EnableTransactionManagement
@MapperScan({"com.src.fivelight.*.dao", "com.src.fivelight.*.*.dao"})
	public class FivelightApplication {

	public static void main(String[] args) {
		SpringApplication.run(FivelightApplication.class, args);
	}

}
