SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- 异业门店表
-- ----------------------------
DROP TABLE IF EXISTS `different_shop`;
CREATE TABLE `different_shop` (
  `shop_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '异业门店id',
  `shop_url` varchar(300) NOT NULL COMMENT '门店封面英文',
  `describe` varchar(300) NOT NULL COMMENT '门店描述',
  `user_name` varchar(255) NOT NULL COMMENT '联系人',
  `shop_phone` varchar(11) NOT NULL COMMENT '联系电话',
  `work_date` varchar(20) NOT NULL COMMENT '上班时间点',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_date` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`shop_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- 用户邀请成功表
-- ----------------------------
DROP TABLE IF EXISTS `invite_user`;
CREATE TABLE `invite_user` (
  `invite_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '邀请id',
  `user_id` int(11) NOT NULL COMMENT '点亮人编号',
  `invite_user_id` int(11) NOT NULL COMMENT '点亮人编号',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_date` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`invite_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- 用户表
-- ----------------------------
DROP TABLE IF EXISTS `user_account`;
CREATE TABLE `user_account` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户编号',
  `open_id` varchar(64) NOT NULL COMMENT '微信用户的唯一标识',
  `nick_name` varchar(25) NOT NULL COMMENT '用户昵称',
  `sex` int(11) NOT NULL COMMENT '用户的性别，值为1时是男性，值为2时是女性，值为0时是未知',
  `province` varchar(15) NOT NULL COMMENT '所在省份',
  `city` varchar(15) NOT NULL COMMENT '所在城市',
  `country` varchar(15) NOT NULL COMMENT '所在城市',
  `headimgurl` varchar(300) NOT NULL COMMENT '微信头像',
  `dream_value` int(11) NOT NULL DEFAULT '0' COMMENT '梦享值',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- ----------------------------
-- 用户地址表
-- ----------------------------
DROP TABLE IF EXISTS `user_address`;
CREATE TABLE `user_address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '地址编号',
  `user_id` int(11) NOT NULL COMMENT '用户编号',
  `user_name` varchar(25) NOT NULL COMMENT '用户名称',
  `user_phone` varchar(11) NOT NULL COMMENT '联系电话',
  `province` varchar(30) NOT NULL COMMENT '省份',
  `city` varchar(30) NOT NULL COMMENT '城市编号',
  `district` varchar(30) NOT NULL COMMENT '地区',
  `garden` varchar(30) DEFAULT NULL COMMENT '小区',
  `building` varchar(30) DEFAULT NULL COMMENT '楼栋',
  `address` varchar(50) NOT NULL COMMENT '地址',
  `lng` varchar(11) NOT NULL COMMENT '经度',
  `lat` varchar(11) NOT NULL COMMENT '纬度',
  `area_type` int(1) NOT NULL DEFAULT '1' COMMENT '区域类型(1.小区,2.门店,3.学校,4.厂区)',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_date` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- 用户共享值表
-- ----------------------------
DROP TABLE IF EXISTS `user_devote`;
CREATE TABLE `user_devote` (
  `devote_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '贡献值编号',
  `user_id` int(11) NOT NULL COMMENT '用户编号',
  `address_id` int(11) NOT NULL COMMENT '地址编号',
  `address_type` enum('1001','1002','1003') NOT NULL DEFAULT '1001' COMMENT '地址类型(''1001:家'',''1002:学校/公司'',''1003:其他'')',
  `devote_value` int(11) NOT NULL DEFAULT '0' COMMENT '贡献值',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_date` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`devote_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- 用户点亮log
-- ----------------------------
DROP TABLE IF EXISTS `user_light_log`;
CREATE TABLE `user_light_log` (
  `light_log_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '点亮日志id',
  `user_id` int(11) NOT NULL COMMENT '用户编号',
  `address_id` int(11) NOT NULL COMMENT '地址编号',
  `click_type` int(1) NOT NULL COMMENT '点击类型(1.点亮自己,2.点亮他人)',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_date` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`light_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- 用户分享表
-- ----------------------------
DROP TABLE IF EXISTS `user_share`;
CREATE TABLE `user_share` (
  `share_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分享编号',
  `user_id` int(11) NOT NULL COMMENT '用户编号',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_date` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`share_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
